# Gitlab Connect slides

## Purpose
Slides for Gitlab Connect talk 2019-04-11

## Usage

Run:
```
./slide-share -dir presentation
```

Goto http://localhost:8080


### Development

```
./slide-serve -dir presentation -dev
```

### Hosted

[Slide on now](https://gitlab-connect-amsterdam.jeroenknoops.now.sh/)

## Slide Share
This presentation uses [Slide-serve](https://github.com/JeroenKnoops/slide-serve).


