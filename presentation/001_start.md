### How we convinced an enterprise 
### company to adopt Gitlab 
###### Philips
###### Jeroen Knoops
@css/orange.css

## Jeroen Knoops
@css/blue.css

## jeroen.knoops@philips.com
@css/blue.css

## I like Programming
@css/blue.css

## Always did
#apple ][
@img/images/apple2.jpg
@css/blue.css

react 
@css/blue.css

react 
react-native 
@css/blue.css

react 
react-native 
rust 
@css/blue.css

react 
react-native 
rust 
java 
@css/blue.css

react 
react-native 
rust 
java 
javascript
@css/blue.css

react 
react-native 
rust 
java 
javascript
docker 
@css/blue.css

react 
react-native 
rust 
java 
javascript
docker 
terraform
@css/blue.css

react 
react-native 
rust 
java 
javascript
docker 
terraform
bash
@css/blue.css

react 
react-native 
rust 
java 
javascript
docker 
terraform
bash
deno.
@css/blue.css

#### Started working in
# 1999

@img/images/witron.jpg

OpenVMS

OpenVMS
Pascal / C

OpenVMS
Pascal / C
Waterfall

OpenVMS
Pascal / C
Waterfall
Client - Server

OpenVMS
Pascal / C
Waterfall
Client - Server
Code reviews

OpenVMS
Pascal / C
Waterfall
Client - Server
Code reviews
One application

OpenVMS
Pascal / C
Waterfall
Client - Server
Code reviews
One application
Big bang releases

OpenVMS
Pascal / C
Waterfall
Client - Server
Code reviews
One application
Big bang releases
Y2K.

#### The internet bubble
# 2000
@img/images/gamegate-2.png
@css/internet.css

Gaming
@img/images/gamegate-1.png
@css/internet.css

Lot of projects 
@css/internet.css

Lot of projects
Java Applets
@css/internet.css

Lot of projects
Java Applets
i18n / l10n
@css/internet.css

Lot of projects
Java Applets
i18n / l10n
WAP
@css/internet.css

Lot of projects 
Java Applets
i18n / l10n
WAP
Continuous Deployments.
@css/internet.css

@img/images/dotcom-bubble-burst.jpg

# Working for
# Philips Medical Systems
@css/blue.css

Meaningful
@css/blue.css

Meaningful
Waterfall
@css/blue.css

Meaningful
Waterfall
Java
@css/blue.css

Meaningful
Waterfall
Java
PVCS
@css/blue.css

Meaningful
Waterfall
Java
PVCS
Send ear-file to ops
@css/blue.css

Finance
Waterfall
Java
PVCS
Send ear-file to ops
Outsource everything to India.
@css/blue.css

# WAT?
@css/yellow.css

# Switched to Philips Research
@css/green.css

## Scrum
@css/green.css

## Scrum
## Ruby on Rails
@css/green.css

## Scrum
## Ruby on Rails
## SVN
@css/green.css

## Scrum
## Ruby on Rails
## SVN
## Sprints
@css/green.css

## Scrum
## Ruby on Rails
## SVN
## Sprints
## Releasing features to end-users
@css/green.css

## Scrum
## Ruby on Rails
## SVN
## Sprints
## Releasing features to end-users
## Removing features
@css/green.css

## Scrum
## Ruby on Rails
## SVN
## Sprints
## Releasing features to end-users
## Removing features
## Integration hell
@css/green.css

## Scrum
## Ruby on Rails
## SVN
## Sprints
## Releasing features to end-users
## Removing features
## Integration hell
## Continuous Integration Systems 
@css/green.css

## Scrum
## Ruby on Rails
## SVN
## Sprints
## Releasing features to end-users
## Removing features
## Integration hell
## Continuous Integration Systems 
## Product moved to business.
@css/green.css

# WAT?
@css/yellow.css

# Joined a Startup
@img/images/gynzy.jpg

## Build one product fast
@css/orange.css

## Build one product fast
## Empowered
@css/orange.css

## Build one product fast
## Empowered
## SVN
@css/orange.css

## Build one product fast
## Empowered
## SVN
## Microservices
@css/orange.css

## Build one product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
@css/orange.css

## Build one product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
@css/orange.css

@img/images/gitlab-ci-contributions.png
@css/orange.css

## Build one product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
## Automation
@css/orange.css

## Build the product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
## Automation
## Continuous Deployment
@css/orange.css

## Build the product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
## Automation
## Continuous Deployment
## Measure everything
@css/orange.css

## Build the product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
## Automation
## Continuous Deployment
## Measure everything
## Monitor everything - Nagios
@css/orange.css

## Build the product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
## Automation
## Continuous Deployment
## Measure everything
## Monitor everything - Nagios
## Show data on dashboards
@css/orange.css

## Build the product fast
## Empowered
## SVN
## Microservices
## Support for both windows and mac
## Gitlab - git svn
## Automation
## Continuous Deployment
## Measure everything
## Monitor everything - Nagios
## Show data on dashboards
## Respond to users.
@css/orange.css

DevOps!
@css/yellow.css

@img/images/devops-1-life-cycle.png
@css/yellow.css

@img/images/devops-2-life-cycle.png
@css/yellow.css

@img/images/devops-3-life-cycle-gl.png
@css/yellow.css

@img/images/devops-4-life-cycle.png
@css/yellow.css

@img/images/devops-5-life-cycle-tools.png
@css/yellow.css

@img/images/devops-6-life-cycle.png
@css/yellow.css

# DevOps is the way to go
@css/yellow.css

# I became a consultant
@css/blue.css

# And I returned to...

@img/images/philips-logo.png

Philips Research

## Healthtech
@img/images/philips-health.jpg
@css/white.css

# Philips is a highly regulated, large 
# and complex environment
@css/story.css

## Philips is a highly regulated, large 
## and complex environment
# It has preferred toolsets,
# WOW and procedures
# because of FDA regulations
@css/story.css

### Philips is a highly regulated, large 
### and complex environment
## It has preferred toolsets,
## WOW and procedures
## because of FDA regulations
# Medical Grade software requires
# support for 15 years
@css/story.css

### It has preferred toolsets,
### WOW and procedures
### because of FDA regulations
## Medical Grade software requires
## support for 15 years
# Philips operates in a lot of
# different areas
@css/story.css

### It has pre-subscribed toolsets,
### WOW and procedures
### because of FDA regulations
## Philips operates in a lot of
## different areas
# Embedded software
@css/story.css

### Philips operates in a lot of
### different areas
## Embedded software
# Machine Learning 
@css/story.css

### Embedded software
## Machine Learning 
# Clinical Platforms
@css/story.css

### Machine Learning 
## Clinical Platforms
# AR/VR
@css/story.css

### Clinical Platforms
## AR/VR
# Connected Sensors
@css/story.css

### AR/VR
## Connected Sensors
# Mobile apps
@css/story.css

### Connected Sensors
## Mobile apps
# Blockchain - Digital Trust
@css/story.css

### Mobile apps
## Blockchain - Digital Trust
# Imaging.
@css/story.css

# Tools
@css/story-tools.css

@img/images/devops-6-life-cycle.png

# Source
TFS
Bitbucket
SVN - Redmine
Jira
IBM Synergy - Change
Clearcase
....
@css/story-tools.css

# Languages
Java
Python
C
C++
C##
MatLab
R
PHP
Ruby
....
@css/story-tools.css

# Frameworks
React
Angular
Panda
React Native
Xamarin
Ionic
Spring Boot
Micronaut
Boots
Tensorflow
Unity
....
@css/story-tools.css

# CI
Jenkins
Own server
Azure
Bamboo
TeamCity
....
@css/story-tools.css

# Deployment
IT managed server
Cloud Foundry
AWS
HockeyApp
Powerpoint
....
@css/story-tools.css

# Communication
Email
Microsoft Teams / Sharepoint
Confluence
Slack
Socialcast
Powerpoint
....
@css/story-tools.css

# Infrastructure
Manual clicking
Bash scripting
Terraform
Ansible
Chef
Cloud Formation
Kubernetes
....
@css/story-tools.css

# WAT?
@css/yellow.css

Define your DevOps lifecycle 
in a highly regulated, 
large and complex environment
@css/orange.css

# Introduce a new tool
@css/define.css

@img/images/gitlab.jpg

Install Gitlab CE
with a shared docker-runner
on a server
  "Easier Jenkins"
@css/define.css

Open Gitlab for your
entire company using LDAP
  "Just a build server"
@css/define.css

Mirror repositories from
other places to Gitlab
  "For CI only"
@css/define.css

Build convenient Docker Images 
for CI and host them on Gitlab
  "For reproducibility"
@css/define.css

Share the code
amongst other projects by
setting default project
*Visibility Level* to
*Public*
@css/define.css

Come-up with a name
for your bunch of scripts
  Forest
@css/define.css

@img/images/forest.png
@css/define.css

Add QA tooling in your
build pipelines
  "Community standard"
@css/define.css

Start using gitlab issues for 
technical issues in your code and 
Supporting Tools
  "Technical Dept stories, 
  not for the real planning tool"
@css/define.css

Start using gitlab
Pages to publish
Test results
@css/define.css

Use these pages,
build pipelines, MRs and
Issue in Quality Audits
  "Comes with our 
  standard pipelines"
@css/define.css

Create run-time docker-images
for production and start 
using gitlab to deploy
  "Immutable builds"
@css/define.css

New projects will now
start on gitlab for their
source
@css/yellow.css

Start questioning
the original VCS and
migrate existing codebases
to gitlab
@css/yellow.css

Start questioning
the original planning tool and
start using Gitlab for new projects
@css/yellow.css

*In 3 years:*
Over 1.000 happy developers
Over 2.000 repositories
Over 150.000 pipelines
Over 400.000 build-jobs
@css/brown.css

Gitlab is now part of the 
preferred toolset with
runners on AWS and kubernetes
@css/brown.css

# Key learnings from Gitlab
### Gitlab updates every 22nd of the month
### The amount of extra features is unbelievable
### If you use Gitlab for a certain feature,
### in a few months new useful features
### are added and easy to use.
*Adopt this way-of-working*
@css/green2.css

# Key learnings from Gitlab
### When something goes wrong, be open about it.
### When Gitlab.com had problems with their database
### backups, they opened a video channel where you
### could see the core team resolving the issue.
*Adopt this way-of-working*
@css/green2.css

# Key learnings from Gitlab
### When you're offering a complete set of
### tools, make them available for everybody.
### Push inner-source principles in the mix.
### Adapt scripts, accept Merge Requests.
### Don't 'own' your scripts.
*People will start re-using things 
if it makes their lives easier*
@css/green2.css

# Key learnings from Gitlab
### Architecture of Gitlab is very stable. 
### Integrations are done with APIs making 
### them NOT breaking other projects.
*This allows for experimentation without
breaking other projects*
@css/green2.css

@img/images/devops-3-life-cycle-gl.png

# Thanks
Ralph
Gert
Jan
Gemma
Donie
Niek 
Gertjan
PPF
@css/blue.css

# QA 

twitter: @johnnybusca
email: jeroen.knoops@philips.com 
*Source: https://gitlab.com/JeroenKnoops/gitlab-connect-amsterdam*
## https://gitlab-connect-amsterdam.now.sh
@css/blue.css
